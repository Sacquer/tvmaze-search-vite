import './style.css'
import { searchInput } from './search.js'

const searchNode = document.querySelector('#search')
searchNode.oninput = searchInput
