import axios from 'axios'
import {
  renderEpisodeList,
  debounce,
  renderListItems,
  episodeInfo,
  resetSearchInput
} from './utils.js'

const say = console.log
const dqsa = (attr) => document.querySelectorAll(attr)

const episodes = new Map()
const URL = `https://api.tvmaze.com/singlesearch/shows?q=`

const searchInput = (event) => {
  let q = event.target.value
  q += '&embed=episodes'
  debouncedTvMazeSearch(q)
}

const episodesPayload = (episodes) => {
  return episodes._embedded.episodes
}

const tvmazeSearch = (query) => {
  axios.get(URL + query)
    .then(res => {
      renderResult(res.data)
      resetSearchInput()
    })
    .catch(err => say(err))
}

const debouncedTvMazeSearch = debounce(tvmazeSearch, 1500)

const renderResult = (data) => {
  episodes.set('episodes', episodesPayload(data))
  let episodesArray = episodes.get('episodes')
  let liNodes = ''

  for (let i = 0; i < episodesArray.length; i++) {
    liNodes += renderEpisodeList(episodesArray[i])
  }
  renderListItems(liNodes)
  attachEventHandlers()
}

const attachEventHandlers = () => dqsa('li.episode-list-item').forEach((element) => element.onclick = renderEpisodeInfo)

const renderEpisodeInfo = (event) => {
  event.preventDefault()
  resetSearchInput()
  document.querySelector('#list-container').innerHTML = episodeInfo(event.target, episodes.get('episodes'))
}

export { searchInput }
