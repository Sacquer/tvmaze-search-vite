const dqs = (attr) => document.querySelector(attr)

const episodeImage = (episode) => episode?.image?.original ?? "https://via.placeholder.com/150.jpg"

const renderEpisodeList = (episode) => {
  let img = episodeImage(episode)
  let id = episode.id
  let name = episode.name
  return `<li class=episode-list-item id=li-${id} data-id=${id}>
        <img src=${img} alt='episode image' height=240 width=352>
        <div>Episode name: ${name}</div>
        <div class='season'>Season number: ${episode.season}</div>
        <div class='episode'>Episode number: ${episode.number}</div>
      </li>`
}

const episodeInfo = (element, episodes) => {
  let episodeId = Number.parseInt(element.dataset.id)
  let [episode, _rest] = episodes.filter((episode) => episode.id == episodeId)
  let img = episodeImage(episode)
  let id = episode.id
  let name = episode.name

  return `<div class=episode-info data-id=${id}>
        <img src=${img} alt='episode image' height=320 width=480>
        <div class='season'>Season number: ${episode.season}</div>
        <div class='episode'>Episode number: ${episode.number}</div>
        <div>Episode name: ${name}</div>
        <div class=episode-summary>${episode.summary}</div>
        <div class=airdate>Airdate: ${episode.airdate}</div>
      </div>`
}

const debounce = (callback, wait = 0) => {
  let timeoutId

  return function(...args) {
    const later = () => {
      clearTimeout(timeoutId)
      callback(...args)
    }

    clearTimeout(timeoutId)
    timeoutId = setTimeout(later, wait)
  }
}


const renderListItems = (listNodes) => dqs('#list-container').innerHTML = listNodes;

const resetSearchInput = () => document.querySelector('#search').value = '';

export {
  renderEpisodeList,
  debounce,
  renderListItems,
  episodeInfo,
  resetSearchInput
}
